/****************************************************************************
** Meta object code from reading C++ file 'bitcoingui.h'
**
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "qt/bitcoingui.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'bitcoingui.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_BitcoinGUI_t {
    QByteArrayData data[59];
    char stringdata0[799];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_BitcoinGUI_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_BitcoinGUI_t qt_meta_stringdata_BitcoinGUI = {
    {
QT_MOC_LITERAL(0, 0, 10), // "BitcoinGUI"
QT_MOC_LITERAL(1, 11, 11), // "receivedURI"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 3), // "uri"
QT_MOC_LITERAL(4, 28, 17), // "setNumConnections"
QT_MOC_LITERAL(5, 46, 5), // "count"
QT_MOC_LITERAL(6, 52, 16), // "setNetworkActive"
QT_MOC_LITERAL(7, 69, 13), // "networkActive"
QT_MOC_LITERAL(8, 83, 12), // "setNumBlocks"
QT_MOC_LITERAL(9, 96, 9), // "blockDate"
QT_MOC_LITERAL(10, 106, 21), // "nVerificationProgress"
QT_MOC_LITERAL(11, 128, 7), // "headers"
QT_MOC_LITERAL(12, 136, 7), // "message"
QT_MOC_LITERAL(13, 144, 5), // "title"
QT_MOC_LITERAL(14, 150, 5), // "style"
QT_MOC_LITERAL(15, 156, 5), // "bool*"
QT_MOC_LITERAL(16, 162, 3), // "ret"
QT_MOC_LITERAL(17, 166, 16), // "setCurrentWallet"
QT_MOC_LITERAL(18, 183, 4), // "name"
QT_MOC_LITERAL(19, 188, 31), // "setCurrentWalletBySelectorIndex"
QT_MOC_LITERAL(20, 220, 5), // "index"
QT_MOC_LITERAL(21, 226, 18), // "updateWalletStatus"
QT_MOC_LITERAL(22, 245, 20), // "handlePaymentRequest"
QT_MOC_LITERAL(23, 266, 18), // "SendCoinsRecipient"
QT_MOC_LITERAL(24, 285, 9), // "recipient"
QT_MOC_LITERAL(25, 295, 19), // "incomingTransaction"
QT_MOC_LITERAL(26, 315, 4), // "date"
QT_MOC_LITERAL(27, 320, 4), // "unit"
QT_MOC_LITERAL(28, 325, 7), // "CAmount"
QT_MOC_LITERAL(29, 333, 6), // "amount"
QT_MOC_LITERAL(30, 340, 4), // "type"
QT_MOC_LITERAL(31, 345, 7), // "address"
QT_MOC_LITERAL(32, 353, 5), // "label"
QT_MOC_LITERAL(33, 359, 10), // "walletName"
QT_MOC_LITERAL(34, 370, 16), // "gotoOverviewPage"
QT_MOC_LITERAL(35, 387, 15), // "gotoHistoryPage"
QT_MOC_LITERAL(36, 403, 20), // "gotoReceiveCoinsPage"
QT_MOC_LITERAL(37, 424, 17), // "gotoSendCoinsPage"
QT_MOC_LITERAL(38, 442, 4), // "addr"
QT_MOC_LITERAL(39, 447, 18), // "gotoSignMessageTab"
QT_MOC_LITERAL(40, 466, 20), // "gotoVerifyMessageTab"
QT_MOC_LITERAL(41, 487, 11), // "openClicked"
QT_MOC_LITERAL(42, 499, 14), // "optionsClicked"
QT_MOC_LITERAL(43, 514, 12), // "aboutClicked"
QT_MOC_LITERAL(44, 527, 15), // "showDebugWindow"
QT_MOC_LITERAL(45, 543, 30), // "showDebugWindowActivateConsole"
QT_MOC_LITERAL(46, 574, 22), // "showHelpMessageClicked"
QT_MOC_LITERAL(47, 597, 17), // "trayIconActivated"
QT_MOC_LITERAL(48, 615, 33), // "QSystemTrayIcon::ActivationRe..."
QT_MOC_LITERAL(49, 649, 6), // "reason"
QT_MOC_LITERAL(50, 656, 21), // "showNormalIfMinimized"
QT_MOC_LITERAL(51, 678, 13), // "fToggleHidden"
QT_MOC_LITERAL(52, 692, 12), // "toggleHidden"
QT_MOC_LITERAL(53, 705, 14), // "detectShutdown"
QT_MOC_LITERAL(54, 720, 12), // "showProgress"
QT_MOC_LITERAL(55, 733, 9), // "nProgress"
QT_MOC_LITERAL(56, 743, 18), // "setTrayIconVisible"
QT_MOC_LITERAL(57, 762, 19), // "toggleNetworkActive"
QT_MOC_LITERAL(58, 782, 16) // "showModalOverlay"

    },
    "BitcoinGUI\0receivedURI\0\0uri\0"
    "setNumConnections\0count\0setNetworkActive\0"
    "networkActive\0setNumBlocks\0blockDate\0"
    "nVerificationProgress\0headers\0message\0"
    "title\0style\0bool*\0ret\0setCurrentWallet\0"
    "name\0setCurrentWalletBySelectorIndex\0"
    "index\0updateWalletStatus\0handlePaymentRequest\0"
    "SendCoinsRecipient\0recipient\0"
    "incomingTransaction\0date\0unit\0CAmount\0"
    "amount\0type\0address\0label\0walletName\0"
    "gotoOverviewPage\0gotoHistoryPage\0"
    "gotoReceiveCoinsPage\0gotoSendCoinsPage\0"
    "addr\0gotoSignMessageTab\0gotoVerifyMessageTab\0"
    "openClicked\0optionsClicked\0aboutClicked\0"
    "showDebugWindow\0showDebugWindowActivateConsole\0"
    "showHelpMessageClicked\0trayIconActivated\0"
    "QSystemTrayIcon::ActivationReason\0"
    "reason\0showNormalIfMinimized\0fToggleHidden\0"
    "toggleHidden\0detectShutdown\0showProgress\0"
    "nProgress\0setTrayIconVisible\0"
    "toggleNetworkActive\0showModalOverlay"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_BitcoinGUI[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      35,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  189,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,  192,    2, 0x0a /* Public */,
       6,    1,  195,    2, 0x0a /* Public */,
       8,    4,  198,    2, 0x0a /* Public */,
      12,    4,  207,    2, 0x0a /* Public */,
      12,    3,  216,    2, 0x2a /* Public | MethodCloned */,
      17,    1,  223,    2, 0x0a /* Public */,
      19,    1,  226,    2, 0x0a /* Public */,
      21,    0,  229,    2, 0x0a /* Public */,
      22,    1,  230,    2, 0x0a /* Public */,
      25,    7,  233,    2, 0x0a /* Public */,
      34,    0,  248,    2, 0x08 /* Private */,
      35,    0,  249,    2, 0x08 /* Private */,
      36,    0,  250,    2, 0x08 /* Private */,
      37,    1,  251,    2, 0x08 /* Private */,
      37,    0,  254,    2, 0x28 /* Private | MethodCloned */,
      39,    1,  255,    2, 0x08 /* Private */,
      39,    0,  258,    2, 0x28 /* Private | MethodCloned */,
      40,    1,  259,    2, 0x08 /* Private */,
      40,    0,  262,    2, 0x28 /* Private | MethodCloned */,
      41,    0,  263,    2, 0x08 /* Private */,
      42,    0,  264,    2, 0x08 /* Private */,
      43,    0,  265,    2, 0x08 /* Private */,
      44,    0,  266,    2, 0x08 /* Private */,
      45,    0,  267,    2, 0x08 /* Private */,
      46,    0,  268,    2, 0x08 /* Private */,
      47,    1,  269,    2, 0x08 /* Private */,
      50,    1,  272,    2, 0x08 /* Private */,
      50,    0,  275,    2, 0x28 /* Private | MethodCloned */,
      52,    0,  276,    2, 0x08 /* Private */,
      53,    0,  277,    2, 0x08 /* Private */,
      54,    2,  278,    2, 0x08 /* Private */,
      56,    1,  283,    2, 0x08 /* Private */,
      57,    0,  286,    2, 0x08 /* Private */,
      58,    0,  287,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::Int, QMetaType::QDateTime, QMetaType::Double, QMetaType::Bool,    5,    9,   10,   11,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::UInt, 0x80000000 | 15,   13,   12,   14,   16,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::UInt,   13,   12,   14,
    QMetaType::Bool, QMetaType::QString,   18,
    QMetaType::Bool, QMetaType::Int,   20,
    QMetaType::Void,
    QMetaType::Bool, 0x80000000 | 23,   24,
    QMetaType::Void, QMetaType::QString, QMetaType::Int, 0x80000000 | 28, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,   26,   27,   29,   30,   31,   32,   33,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   38,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   38,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   38,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 48,   49,
    QMetaType::Void, QMetaType::Bool,   51,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   13,   55,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void BitcoinGUI::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        BitcoinGUI *_t = static_cast<BitcoinGUI *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->receivedURI((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->setNumConnections((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->setNetworkActive((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->setNumBlocks((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QDateTime(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< bool(*)>(_a[4]))); break;
        case 4: _t->message((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< uint(*)>(_a[3])),(*reinterpret_cast< bool*(*)>(_a[4]))); break;
        case 5: _t->message((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< uint(*)>(_a[3]))); break;
        case 6: { bool _r = _t->setCurrentWallet((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { bool _r = _t->setCurrentWalletBySelectorIndex((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 8: _t->updateWalletStatus(); break;
        case 9: { bool _r = _t->handlePaymentRequest((*reinterpret_cast< const SendCoinsRecipient(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 10: _t->incomingTransaction((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const CAmount(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< const QString(*)>(_a[5])),(*reinterpret_cast< const QString(*)>(_a[6])),(*reinterpret_cast< const QString(*)>(_a[7]))); break;
        case 11: _t->gotoOverviewPage(); break;
        case 12: _t->gotoHistoryPage(); break;
        case 13: _t->gotoReceiveCoinsPage(); break;
        case 14: _t->gotoSendCoinsPage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 15: _t->gotoSendCoinsPage(); break;
        case 16: _t->gotoSignMessageTab((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 17: _t->gotoSignMessageTab(); break;
        case 18: _t->gotoVerifyMessageTab((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 19: _t->gotoVerifyMessageTab(); break;
        case 20: _t->openClicked(); break;
        case 21: _t->optionsClicked(); break;
        case 22: _t->aboutClicked(); break;
        case 23: _t->showDebugWindow(); break;
        case 24: _t->showDebugWindowActivateConsole(); break;
        case 25: _t->showHelpMessageClicked(); break;
        case 26: _t->trayIconActivated((*reinterpret_cast< QSystemTrayIcon::ActivationReason(*)>(_a[1]))); break;
        case 27: _t->showNormalIfMinimized((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 28: _t->showNormalIfMinimized(); break;
        case 29: _t->toggleHidden(); break;
        case 30: _t->detectShutdown(); break;
        case 31: _t->showProgress((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 32: _t->setTrayIconVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 33: _t->toggleNetworkActive(); break;
        case 34: _t->showModalOverlay(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (BitcoinGUI::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&BitcoinGUI::receivedURI)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject BitcoinGUI::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_BitcoinGUI.data,
      qt_meta_data_BitcoinGUI,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *BitcoinGUI::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *BitcoinGUI::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_BitcoinGUI.stringdata0))
        return static_cast<void*>(const_cast< BitcoinGUI*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int BitcoinGUI::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 35)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 35;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 35)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 35;
    }
    return _id;
}

// SIGNAL 0
void BitcoinGUI::receivedURI(const QString & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
struct qt_meta_stringdata_UnitDisplayStatusBarControl_t {
    QByteArrayData data[7];
    char stringdata0[88];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_UnitDisplayStatusBarControl_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_UnitDisplayStatusBarControl_t qt_meta_stringdata_UnitDisplayStatusBarControl = {
    {
QT_MOC_LITERAL(0, 0, 27), // "UnitDisplayStatusBarControl"
QT_MOC_LITERAL(1, 28, 17), // "updateDisplayUnit"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 8), // "newUnits"
QT_MOC_LITERAL(4, 56, 15), // "onMenuSelection"
QT_MOC_LITERAL(5, 72, 8), // "QAction*"
QT_MOC_LITERAL(6, 81, 6) // "action"

    },
    "UnitDisplayStatusBarControl\0"
    "updateDisplayUnit\0\0newUnits\0onMenuSelection\0"
    "QAction*\0action"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_UnitDisplayStatusBarControl[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x08 /* Private */,
       4,    1,   27,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, 0x80000000 | 5,    6,

       0        // eod
};

void UnitDisplayStatusBarControl::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        UnitDisplayStatusBarControl *_t = static_cast<UnitDisplayStatusBarControl *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->updateDisplayUnit((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->onMenuSelection((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

const QMetaObject UnitDisplayStatusBarControl::staticMetaObject = {
    { &QLabel::staticMetaObject, qt_meta_stringdata_UnitDisplayStatusBarControl.data,
      qt_meta_data_UnitDisplayStatusBarControl,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *UnitDisplayStatusBarControl::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *UnitDisplayStatusBarControl::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_UnitDisplayStatusBarControl.stringdata0))
        return static_cast<void*>(const_cast< UnitDisplayStatusBarControl*>(this));
    return QLabel::qt_metacast(_clname);
}

int UnitDisplayStatusBarControl::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLabel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
