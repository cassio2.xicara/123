/****************************************************************************
** Meta object code from reading C++ file 'rpcconsole.h'
**
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "qt/rpcconsole.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'rpcconsole.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_RPCConsole_t {
    QByteArrayData data[64];
    char stringdata0[860];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_RPCConsole_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_RPCConsole_t qt_meta_stringdata_RPCConsole = {
    {
QT_MOC_LITERAL(0, 0, 10), // "RPCConsole"
QT_MOC_LITERAL(1, 11, 12), // "stopExecutor"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 10), // "cmdRequest"
QT_MOC_LITERAL(4, 36, 7), // "command"
QT_MOC_LITERAL(5, 44, 8), // "walletID"
QT_MOC_LITERAL(6, 53, 25), // "on_lineEdit_returnPressed"
QT_MOC_LITERAL(7, 79, 27), // "on_tabWidget_currentChanged"
QT_MOC_LITERAL(8, 107, 5), // "index"
QT_MOC_LITERAL(9, 113, 33), // "on_openDebugLogfileButton_cli..."
QT_MOC_LITERAL(10, 147, 29), // "on_sldGraphRange_valueChanged"
QT_MOC_LITERAL(11, 177, 5), // "value"
QT_MOC_LITERAL(12, 183, 18), // "updateTrafficStats"
QT_MOC_LITERAL(13, 202, 12), // "totalBytesIn"
QT_MOC_LITERAL(14, 215, 13), // "totalBytesOut"
QT_MOC_LITERAL(15, 229, 11), // "resizeEvent"
QT_MOC_LITERAL(16, 241, 13), // "QResizeEvent*"
QT_MOC_LITERAL(17, 255, 5), // "event"
QT_MOC_LITERAL(18, 261, 9), // "showEvent"
QT_MOC_LITERAL(19, 271, 11), // "QShowEvent*"
QT_MOC_LITERAL(20, 283, 9), // "hideEvent"
QT_MOC_LITERAL(21, 293, 11), // "QHideEvent*"
QT_MOC_LITERAL(22, 305, 25), // "showPeersTableContextMenu"
QT_MOC_LITERAL(23, 331, 5), // "point"
QT_MOC_LITERAL(24, 337, 23), // "showBanTableContextMenu"
QT_MOC_LITERAL(25, 361, 28), // "showOrHideBanTableIfRequired"
QT_MOC_LITERAL(26, 390, 17), // "clearSelectedNode"
QT_MOC_LITERAL(27, 408, 5), // "clear"
QT_MOC_LITERAL(28, 414, 12), // "clearHistory"
QT_MOC_LITERAL(29, 427, 10), // "fontBigger"
QT_MOC_LITERAL(30, 438, 11), // "fontSmaller"
QT_MOC_LITERAL(31, 450, 11), // "setFontSize"
QT_MOC_LITERAL(32, 462, 7), // "newSize"
QT_MOC_LITERAL(33, 470, 7), // "message"
QT_MOC_LITERAL(34, 478, 8), // "category"
QT_MOC_LITERAL(35, 487, 4), // "html"
QT_MOC_LITERAL(36, 492, 17), // "setNumConnections"
QT_MOC_LITERAL(37, 510, 5), // "count"
QT_MOC_LITERAL(38, 516, 16), // "setNetworkActive"
QT_MOC_LITERAL(39, 533, 13), // "networkActive"
QT_MOC_LITERAL(40, 547, 12), // "setNumBlocks"
QT_MOC_LITERAL(41, 560, 9), // "blockDate"
QT_MOC_LITERAL(42, 570, 21), // "nVerificationProgress"
QT_MOC_LITERAL(43, 592, 7), // "headers"
QT_MOC_LITERAL(44, 600, 14), // "setMempoolSize"
QT_MOC_LITERAL(45, 615, 11), // "numberOfTxs"
QT_MOC_LITERAL(46, 627, 6), // "size_t"
QT_MOC_LITERAL(47, 634, 8), // "dynUsage"
QT_MOC_LITERAL(48, 643, 13), // "browseHistory"
QT_MOC_LITERAL(49, 657, 6), // "offset"
QT_MOC_LITERAL(50, 664, 11), // "scrollToEnd"
QT_MOC_LITERAL(51, 676, 12), // "peerSelected"
QT_MOC_LITERAL(52, 689, 14), // "QItemSelection"
QT_MOC_LITERAL(53, 704, 8), // "selected"
QT_MOC_LITERAL(54, 713, 10), // "deselected"
QT_MOC_LITERAL(55, 724, 23), // "peerLayoutAboutToChange"
QT_MOC_LITERAL(56, 748, 17), // "peerLayoutChanged"
QT_MOC_LITERAL(57, 766, 22), // "disconnectSelectedNode"
QT_MOC_LITERAL(58, 789, 15), // "banSelectedNode"
QT_MOC_LITERAL(59, 805, 7), // "bantime"
QT_MOC_LITERAL(60, 813, 17), // "unbanSelectedNode"
QT_MOC_LITERAL(61, 831, 11), // "setTabFocus"
QT_MOC_LITERAL(62, 843, 8), // "TabTypes"
QT_MOC_LITERAL(63, 852, 7) // "tabType"

    },
    "RPCConsole\0stopExecutor\0\0cmdRequest\0"
    "command\0walletID\0on_lineEdit_returnPressed\0"
    "on_tabWidget_currentChanged\0index\0"
    "on_openDebugLogfileButton_clicked\0"
    "on_sldGraphRange_valueChanged\0value\0"
    "updateTrafficStats\0totalBytesIn\0"
    "totalBytesOut\0resizeEvent\0QResizeEvent*\0"
    "event\0showEvent\0QShowEvent*\0hideEvent\0"
    "QHideEvent*\0showPeersTableContextMenu\0"
    "point\0showBanTableContextMenu\0"
    "showOrHideBanTableIfRequired\0"
    "clearSelectedNode\0clear\0clearHistory\0"
    "fontBigger\0fontSmaller\0setFontSize\0"
    "newSize\0message\0category\0html\0"
    "setNumConnections\0count\0setNetworkActive\0"
    "networkActive\0setNumBlocks\0blockDate\0"
    "nVerificationProgress\0headers\0"
    "setMempoolSize\0numberOfTxs\0size_t\0"
    "dynUsage\0browseHistory\0offset\0scrollToEnd\0"
    "peerSelected\0QItemSelection\0selected\0"
    "deselected\0peerLayoutAboutToChange\0"
    "peerLayoutChanged\0disconnectSelectedNode\0"
    "banSelectedNode\0bantime\0unbanSelectedNode\0"
    "setTabFocus\0TabTypes\0tabType"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_RPCConsole[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      34,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  184,    2, 0x06 /* Public */,
       3,    2,  185,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,  190,    2, 0x08 /* Private */,
       7,    1,  191,    2, 0x08 /* Private */,
       9,    0,  194,    2, 0x08 /* Private */,
      10,    1,  195,    2, 0x08 /* Private */,
      12,    2,  198,    2, 0x08 /* Private */,
      15,    1,  203,    2, 0x08 /* Private */,
      18,    1,  206,    2, 0x08 /* Private */,
      20,    1,  209,    2, 0x08 /* Private */,
      22,    1,  212,    2, 0x08 /* Private */,
      24,    1,  215,    2, 0x08 /* Private */,
      25,    0,  218,    2, 0x08 /* Private */,
      26,    0,  219,    2, 0x08 /* Private */,
      27,    1,  220,    2, 0x0a /* Public */,
      27,    0,  223,    2, 0x2a /* Public | MethodCloned */,
      29,    0,  224,    2, 0x0a /* Public */,
      30,    0,  225,    2, 0x0a /* Public */,
      31,    1,  226,    2, 0x0a /* Public */,
      33,    3,  229,    2, 0x0a /* Public */,
      33,    2,  236,    2, 0x2a /* Public | MethodCloned */,
      36,    1,  241,    2, 0x0a /* Public */,
      38,    1,  244,    2, 0x0a /* Public */,
      40,    4,  247,    2, 0x0a /* Public */,
      44,    2,  256,    2, 0x0a /* Public */,
      48,    1,  261,    2, 0x0a /* Public */,
      50,    0,  264,    2, 0x0a /* Public */,
      51,    2,  265,    2, 0x0a /* Public */,
      55,    0,  270,    2, 0x0a /* Public */,
      56,    0,  271,    2, 0x0a /* Public */,
      57,    0,  272,    2, 0x0a /* Public */,
      58,    1,  273,    2, 0x0a /* Public */,
      60,    0,  276,    2, 0x0a /* Public */,
      61,    1,  277,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    4,    5,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::ULongLong, QMetaType::ULongLong,   13,   14,
    QMetaType::Void, 0x80000000 | 16,   17,
    QMetaType::Void, 0x80000000 | 19,   17,
    QMetaType::Void, 0x80000000 | 21,   17,
    QMetaType::Void, QMetaType::QPoint,   23,
    QMetaType::Void, QMetaType::QPoint,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   28,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   32,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::Bool,   34,   33,   35,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,   34,   33,
    QMetaType::Void, QMetaType::Int,   37,
    QMetaType::Void, QMetaType::Bool,   39,
    QMetaType::Void, QMetaType::Int, QMetaType::QDateTime, QMetaType::Double, QMetaType::Bool,   37,   41,   42,   43,
    QMetaType::Void, QMetaType::Long, 0x80000000 | 46,   45,   47,
    QMetaType::Void, QMetaType::Int,   49,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 52, 0x80000000 | 52,   53,   54,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   59,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 62,   63,

       0        // eod
};

void RPCConsole::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        RPCConsole *_t = static_cast<RPCConsole *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->stopExecutor(); break;
        case 1: _t->cmdRequest((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 2: _t->on_lineEdit_returnPressed(); break;
        case 3: _t->on_tabWidget_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_openDebugLogfileButton_clicked(); break;
        case 5: _t->on_sldGraphRange_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->updateTrafficStats((*reinterpret_cast< quint64(*)>(_a[1])),(*reinterpret_cast< quint64(*)>(_a[2]))); break;
        case 7: _t->resizeEvent((*reinterpret_cast< QResizeEvent*(*)>(_a[1]))); break;
        case 8: _t->showEvent((*reinterpret_cast< QShowEvent*(*)>(_a[1]))); break;
        case 9: _t->hideEvent((*reinterpret_cast< QHideEvent*(*)>(_a[1]))); break;
        case 10: _t->showPeersTableContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 11: _t->showBanTableContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 12: _t->showOrHideBanTableIfRequired(); break;
        case 13: _t->clearSelectedNode(); break;
        case 14: _t->clear((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: _t->clear(); break;
        case 16: _t->fontBigger(); break;
        case 17: _t->fontSmaller(); break;
        case 18: _t->setFontSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->message((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 20: _t->message((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 21: _t->setNumConnections((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->setNetworkActive((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 23: _t->setNumBlocks((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QDateTime(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< bool(*)>(_a[4]))); break;
        case 24: _t->setMempoolSize((*reinterpret_cast< long(*)>(_a[1])),(*reinterpret_cast< size_t(*)>(_a[2]))); break;
        case 25: _t->browseHistory((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 26: _t->scrollToEnd(); break;
        case 27: _t->peerSelected((*reinterpret_cast< const QItemSelection(*)>(_a[1])),(*reinterpret_cast< const QItemSelection(*)>(_a[2]))); break;
        case 28: _t->peerLayoutAboutToChange(); break;
        case 29: _t->peerLayoutChanged(); break;
        case 30: _t->disconnectSelectedNode(); break;
        case 31: _t->banSelectedNode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 32: _t->unbanSelectedNode(); break;
        case 33: _t->setTabFocus((*reinterpret_cast< TabTypes(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 27:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QItemSelection >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (RPCConsole::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RPCConsole::stopExecutor)) {
                *result = 0;
            }
        }
        {
            typedef void (RPCConsole::*_t)(const QString & , const QString & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&RPCConsole::cmdRequest)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject RPCConsole::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_RPCConsole.data,
      qt_meta_data_RPCConsole,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *RPCConsole::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *RPCConsole::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_RPCConsole.stringdata0))
        return static_cast<void*>(const_cast< RPCConsole*>(this));
    return QWidget::qt_metacast(_clname);
}

int RPCConsole::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 34)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 34;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 34)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 34;
    }
    return _id;
}

// SIGNAL 0
void RPCConsole::stopExecutor()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void RPCConsole::cmdRequest(const QString & _t1, const QString & _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
